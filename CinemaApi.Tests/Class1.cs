﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NSubstitute;
using NUnit.Framework;
using RestSharp;

namespace CinemaApi.Tests
{
    [TestFixture]
    public class ApiConnectorTest
    {
        [Test]
        public void AssertThatConnectorReturnsAValidJson()
        {
            var restClient = Substitute.For<IRestClient>();
            
            restClient.Execute(Arg.Any<RestRequest>()).Returns(new RestResponse {Content = TestHelper.GetApiJsonResult()});

            var connector = new ApiConnector(restClient);
            var result = connector.GetResult("&sort_by=release_date.asc&include_adult=false&include_video=false&page=1&primary_release_date.gte=2019-03-04&primary_release_date.lte=2019-03-10");

            Assert.IsFalse(string.IsNullOrEmpty(result), "Http result content should not be empty");
            Assert.DoesNotThrow(() => { JsonConvert.DeserializeObject<JObject>(result); }, "Given http result is not a valid json");
        }
    }

    [TestFixture]
    public class MovieManagerTest
    {
        private  MovieManager _movieManager;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var restClient = Substitute.For<IRestClient>();
            restClient.Execute(Arg.Any<RestRequest>()).Returns(new RestResponse { Content = TestHelper.GetApiJsonResult() });
            _movieManager = new MovieManager(new ApiConnector(restClient));
        }

        [Test]
        public void AssertThatGetMoviesReturnsValidList()
        {
            var movies = _movieManager.GetMovies();

            Assert.IsNotNull(movies);
            Assert.IsNotEmpty(movies);
            Assert.IsTrue(movies.All(movie => !string.IsNullOrEmpty(movie.Title)));
            Assert.IsTrue(movies.All(movie => movie.Popularity != 0.0 && movie.Popularity >= 5));
        }

        [Test]
        public void AssertThatBuiltScheduleIsValid()
        {
            var schedule = _movieManager.BuildSchedule();
            var possibleDays = new List<DayOfWeek>
            {
                DayOfWeek.Wednesday,
                DayOfWeek.Friday,
                DayOfWeek.Saturday,
                DayOfWeek.Sunday
            };
            var possibleHours = new Dictionary<DayOfWeek, List<TimeSpan>>
            {
                {
                    DayOfWeek.Wednesday, new List<TimeSpan>
                    {
                        new TimeSpan(20, 0, 0), new TimeSpan(22, 30, 0)
                    }
                },
                {
                    DayOfWeek.Friday, new List<TimeSpan>
                    {
                        new TimeSpan(20, 0, 0)
                    }
                },
                {
                    DayOfWeek.Saturday, new List<TimeSpan>
                    {
                        new TimeSpan(20, 0, 0)
                    }
                },
                {
                    DayOfWeek.Sunday, new List<TimeSpan>
                    {
                        new TimeSpan(20, 0, 0), new TimeSpan(22, 30, 0)
                    }
                }
            };

            Assert.IsNotNull(schedule);
            Assert.IsNotEmpty(schedule);
            Assert.IsTrue(schedule.All(sch => sch.Movie != null));
            Assert.IsTrue(schedule.All(sch => !string.IsNullOrEmpty(sch.Room)));
            Assert.IsTrue(schedule.All(sch => sch.Date != DateTime.MinValue));
            Assert.IsTrue(schedule.All(sch => possibleDays.Contains(sch.Date.DayOfWeek)));
            Assert.IsTrue(schedule.All(sch => possibleHours[sch.Date.DayOfWeek].Contains(sch.Date.TimeOfDay)));
        }

        [Test]
        public void AssertThatNoMovieIsScheduleIntoRoomsAtTheSameTime()
        {
            var restClient = Substitute.For<IRestClient>();
            restClient.Execute(Arg.Any<RestRequest>()).Returns(new RestResponse { Content = TestHelper.GetApiFiveJsonResult() });
            var manager = new MovieManager(new ApiConnector(restClient));
            var schedule = manager.BuildSchedule();

            var listOfFilms = schedule.Select(sch => sch.Movie.Title).ToList();

            foreach (var film in listOfFilms)
            {
                var movie = schedule.Where(sch => sch.Movie.Title == film).ToList();
                var filmOfCurrentSession = movie.FirstOrDefault();
                movie.Remove(filmOfCurrentSession);

                if (movie.Any())
                {
                    Assert.IsFalse(movie.Any(m => m.Date == filmOfCurrentSession.Date &&
                                                  m.Room != filmOfCurrentSession.Room));
                }
            }
        }
    
    }

    public static class TestHelper
    {
        public static string GetApiJsonResult()
        {
            return File.ReadAllText(@"C:\Dev\CinemaApi\api-result.json");
        }

        public static string GetApiFiveJsonResult()
        {
            return File.ReadAllText(@"C:\Dev\CinemaApi\api-file-results.json");
        }
    }

    [TestFixture]
    public class DateHelperTest
    {
        [Test, TestCaseSource(nameof(GetCaseSource))]
        public void AssertThatGetWeekBorneReturnsFirstAndLastDaysOfWeek(Tuple<DateTime, DateTime, DateTime> dates)
        {
            var borne = DateHelper.GetWeekBoundary(dates.Item1);
            Assert.AreEqual(dates.Item2, borne.Item1);
            Assert.AreEqual(dates.Item3, borne.Item2);
        }

        private static List<Tuple<DateTime, DateTime, DateTime>> GetCaseSource()
        {
            return new List<Tuple<DateTime, DateTime, DateTime>>
            {
                new Tuple<DateTime, DateTime, DateTime>(new DateTime(2019, 05, 03), new DateTime(2019, 04, 29),
                    new DateTime(2019, 05, 05)),
                new Tuple<DateTime, DateTime, DateTime>(new DateTime(2019, 05, 29), new DateTime(2019, 05, 27),
                    new DateTime(2019, 06, 02))
            };
        }
        
        [TestCase(DayOfWeek.Wednesday)]
        [TestCase(DayOfWeek.Friday)]
        [TestCase(DayOfWeek.Saturday)]
        [TestCase(DayOfWeek.Sunday)]
        public void AssertThatReturnedWeekDayIsTheGoodOne(DayOfWeek weekDay)
        {
            Assert.AreEqual(weekDay, DateHelper.GetWeekDay(weekDay, DateTime.Now).DayOfWeek);
        }

        [TestCase(DayOfWeek.Monday)]
        [TestCase(DayOfWeek.Tuesday)]
        [TestCase(DayOfWeek.Thursday)]
        public void AssertThatDateHelperThrowsExceptionOnInvalidDay(DayOfWeek weekDay)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                DateHelper.GetWeekDay(weekDay, DateTime.Today);
            });
        }
    }
}
