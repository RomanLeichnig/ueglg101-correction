﻿using System;
using System.Net;
using RestSharp;

namespace CinemaApi
{
    public class ApiConnector
    {
        private readonly IRestClient _restClient;
        public const string BaseUri = "https://api.themoviedb.org/3/discover/movie?api_key=2834211a08708051f683087dac97dab2&language=fr-fr";

        public const string MovieResourcePath =
                "&sort_by=release_date.asc&include_adult=false&include_video=false&page=1&primary_release_date.gte={0}&primary_release_date.lte={1}"
            ;
        public ApiConnector(IRestClient restClient)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _restClient = restClient;
            _restClient.BaseUrl = new Uri(BaseUri);
        }

        public string GetResult(string resourcePath)
        {
            var restRequest = new RestRequest(resourcePath, Method.GET);
            var response = _restClient.Execute(restRequest);
            return response.Content;
        }
    }
}