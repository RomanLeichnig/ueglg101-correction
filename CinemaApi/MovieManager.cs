using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CinemaApi
{
    public class MovieManager
    {
        private readonly ApiConnector _apiConnector;

        public MovieManager(ApiConnector apiConnector)
        {
            _apiConnector = apiConnector;
        }

        public IEnumerable<Movie> GetMovies()
        {
            var borne = DateHelper.GetWeekBoundary(DateTime.Today);
            var results = _apiConnector.GetResult(string.Format(ApiConnector.MovieResourcePath,
                borne.Item1.ToString("yyyy-mm-dd"), borne.Item2.ToString("yyyy-mm-dd")));
            var jresult = JsonConvert.DeserializeObject<JObject>(results);
            var jmovies = jresult["results"];
            var movies = JsonConvert.DeserializeObject<List<Movie>>(jmovies.ToString());
            foreach (var movie in movies)
            {
                if (movie.Popularity >= 5)
                {
                    yield return movie;
                }
            }
        }

        public IEnumerable<Session> GetAvailableSessions()
        {
            yield return GetSession(DayOfWeek.Wednesday, 20, 0, "room 1");
            yield return GetSession(DayOfWeek.Wednesday, 22, 30, "room 1");
            yield return GetSession(DayOfWeek.Friday, 20, 0, "room 1");
            yield return GetSession(DayOfWeek.Saturday, 20, 0, "room 1");
            yield return GetSession(DayOfWeek.Sunday, 20, 0, "room 1");
            yield return GetSession(DayOfWeek.Sunday, 22, 30, "room 1");
            yield return GetSession(DayOfWeek.Wednesday, 20, 0, "room 2");
            yield return GetSession(DayOfWeek.Wednesday, 22, 30, "room 2");
            yield return GetSession(DayOfWeek.Friday, 20, 0, "room 2");
            yield return GetSession(DayOfWeek.Saturday, 20, 0, "room 2");
            yield return GetSession(DayOfWeek.Sunday, 20, 0, "room 2");
            yield return GetSession(DayOfWeek.Sunday, 22, 30, "room 2");
        }

        private static Session GetSession(DayOfWeek weekDay, int hour, int minute, string room)
        {
            return new Session
            {
                Date = DateHelper.GetWeekDay(weekDay, DateTime.Today).AddHours(hour).AddMinutes(minute),
                Room = room
            };
        }

        public List<Session> BuildSchedule()
        {
            var movies = GetMovies().ToList();
            var result = new List<Session>();
            var availableSessions = GetAvailableSessions();
            var i = 0;

            foreach (var session in availableSessions)
            {
                var movie = movies[i];
                var movieExiste = result.Any(s => s.Movie.Title == movie.Title && s.Room != session.Room &&
                                                    s.Date == session.Date);

                if (!movieExiste)
                {    
                    session.Movie = movies[i++];
                    result.Add(session);
                    i--;
                }

            }

            return result;
        }
    }
}