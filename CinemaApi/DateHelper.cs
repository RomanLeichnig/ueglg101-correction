using System;

namespace CinemaApi
{
    public static class DateHelper
    {
        public static Tuple<DateTime, DateTime> GetWeekBoundary(DateTime today)
        {
            var delta = DayOfWeek.Monday - today.DayOfWeek;
            var firstDayOfWeek = today.AddDays(delta);
            var lastDayOfWeek = firstDayOfWeek.AddDays(6);

            return new Tuple<DateTime, DateTime>(firstDayOfWeek, lastDayOfWeek);
        }

        public static DateTime GetWeekDay(DayOfWeek day, DateTime today)
        {
            var delta = DayOfWeek.Monday - today.DayOfWeek;
            var firstDayOfWeek = today.AddDays(delta);
 
            switch (day)
            {
                case DayOfWeek.Sunday:
                    return firstDayOfWeek.AddDays(6);
                case DayOfWeek.Wednesday:
                    return firstDayOfWeek.AddDays(2);
                case DayOfWeek.Friday:
                    return firstDayOfWeek.AddDays(4);
                case DayOfWeek.Saturday:
                    return firstDayOfWeek.AddDays(5);
                default:
                    throw new ArgumentOutOfRangeException(nameof(day), day, null);
            }
        }
    }
}