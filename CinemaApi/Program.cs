﻿using System;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace CinemaApi
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class Movie
    {
        public string Title { get; set; }
        public double Popularity { get; set; }
    }

    public class Session
    {
        public Movie Movie { get; set; }
        public DateTime Date { get; set; }
        public string Room { get; set; }
    }
}